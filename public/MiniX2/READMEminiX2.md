![ScreenShot](minix2screenshot.PNG)

-press here for fun: https://amanda.hansen1404.gitlab.io/ap2020/MiniX2/
- if the link does not work, go here: https://editor.p5js.org/amanda.hansen1404/full/S-XvKqRy


**Reflection**
____________________________________________________________________________________________________________________________

**Describe your program and what you have used and learnt.**

I struggled with the triangles for the cat's ears and figuring out what to write in the parameters to move and resize the trinagle to fit onto the head of the cat. After a lot of typing, I finally figured out the exact numbers I needed to type in the parameters.

I also really struggled with creating the curve of the mouth on the first emoji. I started over several times before I finally figured out how to get it exactly how I wanted with the help of our instructor.
First, I created an arc I used from the reference page + a few syntax to rotate the mouth. The numbers in the parameters for the arc were super complex and difficult to modify. However, our instructor Anne showed me that you can use the syntax "radians" instead of PI to rotate the arc. This was a lot easier and made much more sense to me. 

I was very determined to make at least one of the emojis interactive - I settled on the lip colour. Even though the lips were very quite difficult to make, I really wanted to add something extra to them. I therefore decided to have the lips change colour every time you pressed on the screen. With the help of Anne and The Coding Train videoes, I created variables to make the lips interactive. I also wanted to challenge myself by using the mousePressed function and I definitely feel like I learnt a lot. 
I really feel like I will use these syntax again and look back on this sketch as I don't feel completely comfortable with using the mousePressed function or variables yet. 
One problem I ran into with the mousePressed function and "else if" and "if" was the number of "{" and "{" to put. I learnt a good tip; be aware of the number of {/} to put - if the syntax all of sudden don't work, add a {/} or remove one. 

I also tried to put whiskers on the cat using the line syntax, though it wouldn't show up on the cat's coordinates. I therefore let it go as I could not figure out the problem and how to fix it.

It was important to me to organize the code and put comments throughout the sketch which I hadn't done the first time. This sketch is much nicer and easier to read - and therefore useful to look back at in the future.

*note*
I originally had an image as a background, but I decided to change it to a solid colour - however, the code is still in my sketch but deactivated.
____________________________________________________________________________________________________________________________


**How would you put your emoji into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend sometimes in thinking about it).**
	
The creative process for this MiniX was more difficult than the other one. There was a bit more pressure to create something cool and meaningful, especially since the first MiniX was our first and the expections low. I focused more on the technical part of the assignment rather than the creative as I wanted to create something cool and challenge myself to use syntax I had not used before. 
However, I had a clear focus for the creative part. The emojis I created were inspired by me and not so much about culture. However, the second emoji has a theme many can relate to - being tired and sleepy and having long days. I think .this fits into university culture. 
Overall, this was an exploration and is not to be taken too seriously. I expressed myself and challenged myself technically. 

*Examples:*
https://p5js.org/reference/#/p5/arc - I used to arc reference for the lips and eye lids of my emojis and modified it to my liking. 
https://p5js.org/examples/form-shape-primitives.html - I used this example to create the curve/smile on the lips
https://p5js.org/reference/#/p5/text - I used this reference to create the text
