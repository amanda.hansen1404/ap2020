//var bg;

var lipcolour;
var count = 0;
//function preload(){
  //    bg = loadImage("79516749-cat-background.jpg")

function setup() {
  // put setup code here
  createCanvas (windowWidth, windowHeight);
  lipcolour = color(230,0,0);
}

function draw() {
  //background(bg);
  // put drawing code here
  background(200,150,180);

  //Emoji number 1

  //head
  noStroke();
  fill(255, 204, 0);
  circle(200,120,210)

  //lips
  //top lip
  fill(lipcolour);
  arc(180, 170, 40, 17, PI, TWO_PI);
  arc(220, 170, 40, 17, PI, TWO_PI);
  //bottom lip
  arc(200, 170, 80, 35, 0, PI);

  //smile
  stroke(0);
  strokeWeight(2);
  noFill();
  arc(200, 117, 160, 120, radians(45), radians(135));

  //eyes
  // Eye balls
  noStroke();
  fill(255);
  ellipse(160, 100, 40);
  ellipse(240, 100, 40);

  // Pupils
  noStroke();
  fill(0);
  ellipse(155, 105, 20);
  ellipse(235, 105, 20);

  //cheeks
  noStroke();
  fill(250,0,0,50);
  ellipse(140, 139, 30);
  ellipse(260, 139, 30);

  //cat
  fill(100);
  circle(66,150,60);
  //left ear
  fill(100);
	triangle(37,150,48,105,60,125);
  fill(250, 150, 150);
  triangle(45,126,49,111,55,123);

  //right ear
  fill(100);
	triangle(60,150,80,105,90,134);
  fill(250, 150, 150);
  triangle(75,123,80,111,85,126);

  //eyes
  fill(255, 250, 0);
  strokeWeight(1);
  stroke(0);
  ellipse(52, 143, 10,6);
  ellipse(78, 143, 10,6);
  //pupils
  noStroke();
  fill(0);
  ellipse(52, 143, 5);
  ellipse(78, 143, 5);

  //nose
  fill(250, 150, 150);
  ellipse(65, 156, 7,5);

  //mouth
  fill(0);
  arc(65, 166, 16, 12, 0, PI);

  //whiskers
  line(250,150,260,180);

  //Emoji number 2
  //head
  noStroke();
  fill(255, 230, 0);
  circle(450,120,210)

  //eyes
  //eye ball
  strokeWeight(3);
  stroke(0);
  fill(1000);
  ellipse(410, 100, 40);
  ellipse(490, 100, 40);

  //red eye effect
  fill(250,0,0,50);
  ellipse(410, 100, 40);
  ellipse(490, 100, 40);

  //iris
  noStroke();
  fill(10,80,250);
  ellipse(410, 100, 25);
  ellipse(490, 100, 25);

  //pupil
  fill(0);
  ellipse(410, 100, 15);
  ellipse(490, 100, 15);

  //light reflection
  fill(1000);
  ellipse(412, 100, 8);
  ellipse(492, 100, 8);

  //eye lid
  fill(0);
  arc(410, 100, 40, 40, PI, TWO_PI);
  arc(490, 100, 40, 40, PI, TWO_PI);

  //eyebrows
  stroke(0);
  strokeWeight(2);
  noFill();
  //left brow
  line(385,85,420,65);
  //right brow
  line(475,65,515,85);

  //mouth
  noStroke();
  fill(0);
  ellipse(450, 175, 50,35);

  //text
  fill(0);
  textSize(20);
  textStyle(ITALIC);
  text('I am so tired', 480, 60);

  text('press me', 250, 190);

  textSize(40);
  textStyle(BOLD);
  fill(0);
  stroke(0);
  strokeWeight(2);
  text('@ home', 125,275);
  text('@ uni', 395, 275);
  fill(200,50,150);
  text('@ home', 123,272);
  text('@ uni', 393, 272);


}
  function mousePressed() {
    if (mouseX>159 && mouseX<239 && mouseY>160 && mouseY<189){
    count = count + 1;
    if (count==1) {
    lipcolour = color(200,0,100);
  } else if (count==2) {
    lipcolour = color(200,100,100);
  } else if (count ==3) {
    lipcolour = color(50,100,200);
  } else if (count ==4) {
    lipcolour = color(230,0,0);
    count = 0
  }

  }
}
