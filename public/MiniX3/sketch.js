var time = ["press the mouse","do ten ab crunches", "do ten jumping jacks", "tired yet?", "do ten pushups", "stare into the screen until I finish", "are you bored?", "run around the room for one minute", "let's go again" ]
var index = 0
var textcolour = ['rgb(94,166,68)','rgb(244,231,112)','rgb(204,112,244)','rgb(244,112,114)','rgb(244,134,112)']
var speed = 4
var clockcolour = ['black','white']

function preload() {
 music = loadSound('WorkoutMusic.mp3');
}
function setup() {
  createCanvas(windowWidth, windowHeight);
  music.setVolume(0.1);
  music.play();
}

function draw() {
  background(189,220,223);

  push();
  //throbber text
  frameRate(7);
  fill(random(clockcolour));
  textSize(60)
  textFont('Georgia');

  //making the text move like a clock
  translate(width/2,height/2);
  rotate(radians(frameCount*speed));
  text("Don't", 0,0);
  pop();

  push();
  frameRate(7);
  fill(random(clockcolour));
  textSize(60)
  textFont('Georgia');
  translate(width/2,height/2);
  rotate(radians(frameCount*speed+90));
  text("waste", 0,0);
  pop();

  push();
  frameRate(7);
  fill(random(clockcolour));
  textSize(60)
  textFont('Georgia');
  translate(width/2,height/2);
  rotate(radians(frameCount*speed+180));
  text("your", 0,0);
  pop();

  push();
  frameRate(7);
  fill(random(clockcolour));
  textSize(60)
  textFont('Georgia');
  translate(width/2,height/2);
  rotate( radians(frameCount*speed+270) );
  text("time", 0,0);
  pop();

  //text following the mouse
  frameRate(7);
  fill(random(textcolour));
  textSize(40);
  text(time[index], mouseX, mouseY);

}
//changing the text when the mouse is pressed
function mousePressed() {
  index = index + 1;

  if (index==9) {
    index = 0;
  }
}
