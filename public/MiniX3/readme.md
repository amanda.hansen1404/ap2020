![ScreenShot](minix3screenshot.PNG)

**Link:** https://amanda.hansen1404.gitlab.io/ap2020/MiniX3/ (Remember to turn on the **sound**)


**Reflection:**

**What is your sketch? What do you want to explore and/or express?**
With my sketch, I wanted to challenge what a traditional throbber looks like - I wanted to create a new take on it. My first initial thought was to create an animated figure of sorts, but I decided it would be too difficult for me. Instead, I decided on doing a simple throbber which requires more from the user that a normal throbber would. I was inspired by Winnie's text on the throbber to focus on the "waiting" part. I was also inspired by the Metainterface text we read in which the artworks mentioned criticise and mock digital platforms/programs. I wanted to do something similair and decided to combine my inspiration from these two texts - making fun of how much time we actually spend on waiting for something to load - and probably do nothing in these "waiting moments". With my throbber, I want to encourage taking advantage of the waiting by encouraing people to be active. I also added workout music to create a bit of fun - this was also to challenge myself and to see if I would be able to add sound which I had failed to do in the past. 
To put it shortly, I wanted to criticise how much time we spend on waiting without knowing for how long. 

**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your process of coding)?**
I spent a lot of time searching for and deciding which time-reated syntaxes/functions I needed to use to bring my vision to life. I used frameRate for the blinking colour-changing effect for the text - I used this to change the speed of how the colour changes. I played around with chaning the frame rate in order to get the desired blinking-effect. 
I also used frameCount inside the rotate-syntax used to create the clock-like text in the center of my canvas. This was to create the clock-effect and make each line of text start at a different place. 
Example: rotate(radians(frameCount));
I decided to make a clock made of text to add another time-element and to play into the idea of how time matters when throbbers appear. Also, as mentioned before, I was inspired by the Metainterface text to criticise the whole notion of waiting for an unknown amount of time.  

I considered using the loop-function as well to add even more lines of text to my clock, but I eventually decided it wasn't necessary. 

**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterise this icon differently?**
I think it is very simple what a throbber communicates - that you need to wait for something to load. It definitely hides for how long you actually have to wait and therefore if it even is worth waiting - because you might waste your time if it takes too long. 
When I encounter throbbers, I really don't think much of it - I wait for a little while until I get impatient and do something else. I usually check my connection to see if the problem is related to bad connection. How others view the icon might be different, but I think everyone can agree on the fact that waiting is annoying. It depends on what you are waiting on - if it's something like watching a video or something more important like buying a train ticket before it is too late. 

**The final result:**
This is the first sketch that I am actually kind of proud of - I really like my idea and how it turned out. Even though it is very simple and not advanced at all, I am happy with it. I didn't want to just copy a random throbber sketch that I found online and modify it - I wanted to find and add all the syntax myself and create my throbber from skratch. This was also to find out what I was capable of (with help from the instructor, Ann). That being said, I did look through many examples to find out which syntax could be used to bring my idea to life. I also used Daniel Shiffman's video on arrays for the changing text colour in my sketch.
Most importantly, I had a lot of fun coming up with this sketch.

**Examples/references:**
Rotating the text: https://editor.p5js.org/aferriss/sketches/HyBagyadG
The text colour array: https://www.youtube.com/watch?v=VIQoUghHSxU