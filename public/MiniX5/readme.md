## Revisit the past

**Link:** https://amanda.hansen1404.gitlab.io/ap2020/MiniX5/

**Link to the original miniX:** https://amanda.hansen1404.gitlab.io/ap2020/MiniX1/

### Minix5
![ScreenShot](screenshot5.PNG)

### Minix1
![ScreenShot](screenshot1.PNG)


## Reflection

**What is the concept of your work? + what is the departure point?**

I have based this MiniX on the first MiniX as there was a lot to improve. However, the final result is quite different as I have changed the first sketch completely. The idea behind it is the same;
I wanted to create something fun inspired by the "cultural phenomenon" of memes; I was inspired by this meme:

![ScreenShot](fighter meme.png)

Memes are something I spend a lot of time looking at and laughing at. I was inspired by the "choose your fighter" meme when I decided to used the web-cam syntax.
I wanted to challenge myself technically but that wasn't really my focus point - I just wanted to include a part of my culture that I find fun. Also, I was inspired by Snapchat filters
which are also a part of the culture of my generation. I therefore wanted to simulate a filter in my program by adding the web-cam function and inserting images. I found the images on Google and
created the transparent background myself so only the faces of the cates very visible in the execution of the code. 


**What do you want to express?**

I really wanted to express fun and encourage the "viewer" to participate in the program - by choosing their cat and inserting themselves behind one of the cat faces.
I thought of implementing a larger cultural meaning, but I decided it wasn't necessary - code and programs don't always have to be critical, they can just be fun for the sake of it.


**What have you changed and why?**

In the original MiniX, I mainly used shapes and text and added variables to get the text to shake. The text was quite simple except a lot of the syntax in the code were pointless as I didn't quite understand how they worked. 
I also did not know how to change the colour of each element without colouring every shape and all of the text. 
This MiniX is more advanced, more organized, and makes much more sense to me. The main difference is that I used a dom elemt; web-cam, and added more text and more images. I have also added an interactive element with the keyPressed-function. 
I added this function so when you press 's', a screenshot is taken. hHowever, I realized that it doesn't screenshot what the web-cam displays, but only a white background and the images + text. I do not know how to change this, but I decided to keep it anyways.
Also, I created my own cursor by using the noCursor syntax and adding an image that follows the mouse. Fun fact, the cursor is my cat.

Even though the two sketches are much different, it was important for me to reuse the basic idea behind it - fun with memes. The new sketch is therefore based on the same idea yet only executed differently as I have gained a lot of knowledge since then.


**Reflect upon what Aesthetic Programming might be (Based on the readings that you have done before (especially this week on Aesthetic Programming as well as the class01 on Why Program? and Coding Literacy), What does it mean by programming as a practice, or even as a method for design? Can you draw and link some of the concepts in the text and expand with your critical take? What is the relation between programming and digital culture?)**

The aesthetic aspect of programming is quite difficult to define, especially since aesthetics in this sense does not mean beauty as it traditionally does. 
However, after a few weeks of learning about the course, I have an idea of what it means. I believe aesthetic programming has something to do with experience - it is not mainly about the technicalities of code and how well you can program. 
It is also about the thought process behind the code and what you want to express, and the cultural impact the program might have. For example, even if you might not be the greatest at programming, in order to send a certain message or bring a certain idea to life - you might be forced to be creative to work around your limitations. 
I think there is real value in finding simpler alternatives to create certain things in your code if you are not very advanced. 
As mentioned in the preface of Winnie Soon's software handbook, aesthetic programming is about being creative and experimental. It also about "building things and producing reflexive work of critique". 
This supports my argument of how aesthetic programming has something to do with the thought process behind it and its potential impact on culture - as well as what it means for yourself. It therefore seems as if you need to be critical of your program and the world around you.

Aesthetic programming is perhaps also related to cultural development - as in how coding has become a very used skill and is considered as important as reading and writing. According to the Coding Literacy text by Anette Vee, coding offers new ways to think and empowers individuals - it gives you opportunites in employment and perhaps also personal development. Again, thinking seems to be an important aspect in this text as well. 
New ways of thinking add to aesthetics and the "beauty" of what you want to express and how. 

Overall, I would say the aesthetic aspect of programming refers to aesthetics of thinking- what you program and why it matters. It is about what you want to express and how you choose to express it. Even though Winnie Soon mentions in the preface of her handbook that aesthetics doesn't mean beauty in this case - I believe it can still be considered beautiful to be considerate and thoughtful when you program. 
There is beauty in having the power to create things and using your programming skills to be critical of the world. 


**Examples/references:**
https://editor.p5js.org/enickles/sketches/S1UVsu2yG - screenshot function