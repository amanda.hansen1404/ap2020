//image variables
var cat;
var sadcat;
var happycat;
var catmouse;
saveCount = 0;

let capture;

function preload() {
  //loading the images
  cat = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/88080216_901122870337162_4533290603143757824_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_ohc=tqWTk0uA2F8AX8NE2vt&_nc_ht=scontent.faar2-1.fna&oh=713517483ed5647700a1131d9a0fbae2&oe=5E995274");
  sadcat = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/88098187_896899037411350_7390104034406825984_n.png?_nc_cat=108&_nc_sid=b96e70&_nc_ohc=CYWnkoAUX1UAX8P9Cv9&_nc_ht=scontent.faar2-1.fna&oh=165c7f1b7fef11cb366eada93d06814b&oe=5E838CCF");
  happycat = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/89018196_948257178938194_5773590408479113216_n.png?_nc_cat=105&_nc_sid=b96e70&_nc_ohc=I4QH7w--N6kAX9jYgwW&_nc_ht=scontent.faar2-1.fna&oh=68f1a8dffd6edcd6aca0eb804c7478b2&oe=5E9815EF");
  catmouse = loadImage("https://scontent-arn2-1.xx.fbcdn.net/v/t1.15752-9/88125265_202923557737393_7956705637742673920_n.png?_nc_cat=102&_nc_sid=b96e70&_nc_ohc=go2JkbxFLBIAX-Uc8Dh&_nc_ht=scontent-arn2-1.xx&oh=5058ecb5693748d6c3a37d6bd856f56e&oe=5E8475F2")

}

function setup() {
  //activating the web cam
  capture = createCapture(VIDEO);

  //making the cursor  invisible
  noCursor();

}

function draw() {
  background(1000);

  //setting up the web cam
  capture.size(550, 550);
  let c = createCanvas(550, 550);
  c.position(0,0);

  // header
  fill(0);
  textSize(50);
  textStyle(BOLDITALIC);
  textAlign(CENTER)
  text('choose your fighter', 275, 45);

  //left cat
  happycat.resize(150, 0);
  image(happycat, 10, 100);

  //middle cat
  sadcat.resize(160, 0);
  image(sadcat, 200, 115);

  //right cat
  cat.resize(170,0);
  image(cat, 370, 110);

  //question
  push();

  //creating the 3D effect of the text
  fill(0);
  textSize(30);
  textStyle(ITALIC);
  textAlign(CENTER)
  text('are you happy, sad or annoyed?',273,378);

  //light pink text
  fill('rgb(249,176,158)');
  textSize(30);
  textStyle(ITALIC);
  textAlign(CENTER)
  text('are you happy, sad or annoyed?',275,380);

  pop();

  //mouse
  catmouse.resize(40,0);
  image(catmouse,mouseX,mouseY);

  fill(0);
  textSize(15);
  textAlign(CENTER)
  text("press 's' to screenshot",273,468);

  fill(1000);
  textSize(15);
  textAlign(CENTER)
  text("press 's' to screenshot",275,470);

}
function keyPressed() {
  if (key == 's') {
    save("screenshot" + saveCount + ".png");
    saveCount++;
  }
}
