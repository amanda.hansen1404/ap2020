## Mini Ex 10
This readme contains a link to the group readme and flowcharts + the individual readme and flowcharts

### Group 8 - flowchart
**Link to the group readme:** https://gitlab.com/KatrineHK/ap2020/-/tree/master/public/MiniX10

## The individual flowchart + reflection
**Link to the MiniX:** https://amanda.hansen1404.gitlab.io/ap2020/MiniX4/

**Link to the repository of the MiniX:** https://gitlab.com/amanda.hansen1404/ap2020/-/tree/master/public/MiniX4


**The flowcharts**

I decided to create two flowcharts for what I believe is the most technically challenging of my individual MiniEx's - Mini Ex 4. 
I have created a technical flowchart and a conceptual flowchart as it gives a more well-rounded understanding of my program without reading the source code.
That way, you see the flow of the program from a programmer's point of view and the point of view of the user. I had a bit of trouble deciding if I had to create two.
In the text "The multiple meanings of a flowchart", it is mentioned that a flowchart is a central design document for communication between programmers, analysists, and users - It therefore made sense to me to create two different flowcharts.

(scrool down for the flowcharts)

**What is the value of the individual flowchart that you have produced?**

As this exercise involved an old program that I left behind many weeks ago, it was quite an interesting experience to create flowcharts for it.
I had a hard time imagining what to use it for, but I realised that the value lies in the process of making them. I had a lot of different thoughts and considerations when creating the two flowcharts.
Which elements were cruicial, and which would be redundant to include? I found it especially difficult to decide on where the "no" branches were supposed to go when using the diamond shape for a decision point. 
The "yes" branch is obviously easy to connect, but truly understanding each part of the program is much harder than I anticipated. When does a program really end, and are there only a certain flow one can go through to end at a certain place?
The process of making the flowchart made me reflect on this question as well as the order of my own program. I find this a good thing as flowchart can be a tool for critical thinking. However, it was a struggle to create them, and I am still not sure what a flowchart should really look like.
The text on flowcharts also mentions that the program code can sometimes be easier to understand than the flowchart - and I believe this is the case here. However, I have realised from reading the text that perhaps the understanding of flowcharts is not a universal thing. It can mean different things to different people.
It truly depends on who and what you intend to use it for.


### The technical flowchart
![ScreenShot](MiniX10indiflow-tech.png)

### The conceptual flowchart
![ScreenShot](MiniX10indiflow-concept.png)
