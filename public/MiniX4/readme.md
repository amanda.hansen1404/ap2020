### BE FREE

![ScreenShot](screenshot4.PNG)

**Link:** https://amanda.hansen1404.gitlab.io/ap2020/MiniX4/

____________________________________________________________________________________________________

### Reflection:


**Provide a title of your work and a short description of the work (within 1000 characters) as if you are submitting to the festival open call.**

BE FREE is about life in the digital world. It aims to express how those of us involved in digital culture are exposing ourselves to some degree. When you enter a site, you are asked to accept cookies.
When we wander the internet, we expose details about ourselves and sacrifice some privacy. I based **be free** on "secrets" and was inspired by how we show more of ourselves than we might realize. I wanted to express 
this idea through my code - and I put emphasis on this by exaggerating it. When revealing a part of yourself that you might want to stay hidden - by typing
in your secret - you are told that the site already know this. I did this to exaggerate how large a part algorithms, data mining, and so on plays in your life.
I don't know how much the internet knows about me, I only know what I actively put out - but I do know that the internet knows much more about me than I can imagine. It was therefore
important for me to emphasise this in my work as I know I am probably not alone with these thoughts.


**Describe your program and what you have used and learnt.**

The idea was to create a sketch that requires input from the viewer/user - and I decided on text, mouse, and key input. I felt this would bring my idea to life the best - I had no idea how I could create something cool by using audio or web camera input. I created an input box and a button to simulate a real text box that requires action from the user in order
to be sent into the world. I experimented with the style of these two. I decided on using a white background, black text, and a dark red button - I wanted the site to look sketchy by making it almost too simple. 
I also created my own function - not to make my code more simple, but just to try it out and see if I could do it.
Furthermore, I wanted to create a "secret" message of some  sort - so when you type your secret, the words "be free" changes into "you will never truly be free".
Lastly, I created a mouseClicked function to add the "capture all" aspect that requires action from the user.


**Articulate how your program and thinking address the theme 'capture all'.**

My program and the idea behind it address the theme 'capture all' well, I believe. **Be free* is built on the idea of the web "capturing" data from users - that means almost everybody is affected by it to some extent.
Furthermore, it aims to "capture" a secret from the user which requires them to actively write something in the input box. 


**What are the cultural implicatons of data capture?**
The cultural implications are, for example, how we expose details about ourselves that we actually might not want to be exposed. When we are a part of digital culture, we become more vulnerable
and are forced to sacrifice details about ourselves. The biggest problem with this is not the amount of data we contribute with, I believe, it is how we are barely aware of it.
We cannot really know how little or how much privacy we have - and that can be quite concerning. Especially since it is difficult to do anything on the internet without sacrificing details about ourselves.
