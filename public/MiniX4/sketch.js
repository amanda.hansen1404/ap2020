
  var words = ["be free","you will never truly be free"]
  var index = 0

function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight);

  var button;
  var input;

  //the text input box
  push();

  input = createInput('',"password");
  input.position(180,151);
  input.style('width','118px');
  input.style('height','16px');
  input.value('');
  pop();
}

function draw() {
  // put drawing code here
  background(1000);

  //calling the function
  secret();

  //mouse text
  textSize(10);
  text("you are not alone", mouseX, mouseY);

}

function secret() {
  //header
  textSize(32);
  if(keyIsPressed === true) {
    index = 1
  } else {
    index = 0
  }
  text(words[index],220,100);

  //header for the input box
  textSize(15);
  text("tell me a secret",180,148);

  //the button for the text box
  push();

  button = createButton('Reveal');
  button.position(300,150);
  button.style('background','rgb(188,51,51)');
  button.style('font-size', '15px')
  button.style("font-family", "Bodoni");
  pop();

}

//alert when clicking on the button
function mouseClicked() {
  if (mouseX >= 300 && mouseX <= 355  && mouseY >= 148 && mouseY <= 175) {
    alert('we already know this about you');
  }
}
