var bg;
var x = 150;

function preload(){
    bg = loadImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRSURc1J5tJqylGpUjB25D0ONiIemRAQ99lH2pwzupnRrECkeH6")
}
function setup() {
createCanvas(300,300)
  background('rgba(0,255,0, 0.25)');
}

function draw() {
  background(bg)
  x = x+random(-2,2);

  strokeWeight(0);
  strokeCap(ROUND);
  circle(69, 135, 32);
  let c = color('#0f0'); // Define color 'c'
  fill(c);
  let a = color('pink');
  fill(a); // Use 'c' as fill color
  noStroke(); // Don't draw a stroke around shapes

  fill(a); // Use 'c' as fill color
  textSize(32);
  textStyle(BOLDITALIC);
  text('This kitty is cold', 10, 30);
  stroke('black');
  circle(200, 140, 32);
  textSize(10);
  text('brrrrrrrr', x, 200);
  }
