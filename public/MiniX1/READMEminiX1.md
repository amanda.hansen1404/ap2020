![ScreenShot](minix1screenshot.png)

link: https://amanda.hansen1404.gitlab.io/ap2020/MiniX1/

My first coding experience was off to a rough start as I had problems with Atom and getting it to work. However, when I finally did figure out the program and getting the syntaxes to work, I started experimenting and having fun.
It was difficult to figure out how to work with the syntaxes. It took a lot of experimentation and random typing. I am still unsure of what certain parameters of certan syntaxes mean and how changes affect them. This is something I am sure I will figure out as I get more experience with the program and watch more videos about p5.js. Eventually, I will know which syntaxes to use and how to use them properly.
I especially struggled with changed the colour of single shapes without changing the colour of other shapes. As some point, I thought I figured it out, but it later turned out I didn't. Also, knowing where to put syntaxes was really difficult and took a lot of attempts.
My final product ended up being totally different from when I first started. When I figured out how to set an image as the background, I deleted most of my shapes and changed their coordinates and some text.
I thought it was a really frustrating but fun process. It gives me hope that I will eventually learn how to code. The coding process and Atom reminded me of a math program I used at school which used commands instead of lines of code. I realized the two are very similar and it made the coding in Atom look less strange.
I tried to approach coding without being too intimated but more excited. The texts we read for the first class inspired me to take on coding with excitement as it seems to be a valuable skill to have. I therefore want to take advantage of this and try my best to become good at coding.
