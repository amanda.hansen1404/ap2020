## Mini Ex 6

**Link:** https://amanda.hansen1404.gitlab.io/ap2020/MiniX6_2/

![ScreenShot](screenshot6.PNG)

## Reflection

**Describe how does your game/game objects work?**

This game is about a chicken that needs to avoid the curry bowls and avoid becoming a part of the dish. 
The user has to control the chicken by using the arrow keys to move it around the canvas and avoid the curry bowls moving by. I have not considered adding levels or perhaps a maximum of curry bowls hits - I would just like a counter to be displayed on the screen. 

The more you avoid, the better you are at the game. There are no levels or end goal - It is up to the user when they are happy with their result. 
I also did not want to overcomplicate the code and make it unrealistic for myself to create.
If I could do it, I would add a counter of curry bowls avoided and one of curry bowls hit. Unfortunately, I did not figure out how to do this. 


Winnie's sketch and lecture about object abstraction inspired me to add the food aspect - and my favourite food is green curry. I therefore chose to make this a part of my game.

**Describe how you program the objects and their related attributes and methods in your game.**
	
I was inspired by Winnie's tofu game but replaced tofu with curry bowls - except for catching the curry bowls, I want the game to be about avoiding the curry bowls. 
The element of having to avoid something reminds of my childhood and the games I played. Back then, games were simpler and with a simple objective. 
I wanted my game to be simple as well - this is also because of my limited coding skills.

I copied a few of the blocks of code from Winnie's tofu game as I had no idea at all how to do it from skratch by myself.
Unfortunately, this means I do not understand every single line - I only understand their collective effect on the program. 
I am not happy about this - it is important to me that I actually understand what I am doing and why I am doing it. But I have to be honest with myself and accept that I do not have the skills to create an advanced game myself. 
I spent hours trying to fix very simple things and correct my mistakes - this, unfortunately, proved unsuccesful in most cases. 
But at least I tried, and at some point, I might understand it much better than I do now.

If I had more time, I would most likely have created a completely different game that I would have been able to create and develop by myself. 
Unfortunately, I have to upload an unfinished and faulty program. 


**What are the characteristics of object-oriented programming and the wider implications of abstraction?**

I think this subject is very difficult to grasp, especially the essay by Fuller. Object-oriented programming is obviously centered around objects and how they interact with computation. 
As Fuller mentions in How To Be A Geek, computational objects have power. I believe this power has something to do with representation. 
"Objects are created as instances of ideal types." - When you program and use computated objects, you decide what is most important among their many attributes and behaviour. 
What you leave out and choose to use will most likely affect the message it sends. 

When you assign attributes to an object, you are forced to decide what that object means to you and others. At the same time, the abstractation of the object is basically removed as the object becomes simplified. 
Since you need to choose to attributes yourself that you want to assign to the object, it becomes simplified as details and perhaps other attributes are left out.
I would argue that this makes object-oriented programming subjective. 

In my case, I do not believe there are any wider implications of abstraction - it is not that deep, especially since the only attributes I have given my objects - images - are speed, size, and position. 
The fact that I have just used images as opposed to creating the objects from scratch, and have therefore not left out any attributes, means the cultural implications are very limited. 

**Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted?**
	
As mentioned before, there aren't really any wider cultural implications as far as I can see. 
I didn't really have the problem of choosing/leaving out any elements of the objects in my program. 
I was also not forced to decide between what attributes I decided were most important of each objects. 
However, since I used images instead, perhaps the act of choosing which images to use is a form of choosing what is important and not important enough to be displayed - as mentioned in the Fuller essay.

When connecting my game to a wider digital culture context, you can perhaps consider my choice of the "avatar" - the chicken - and the obstacles - the curry bowls. I did wonder if it was offensive to use a chicken - is it disrespectful to vegans/vegetarians or even animals?
Regardless of which avator or which obstacles I chose to use, the game would be the same - but of course, the cultural element is also important to consider. You need to ask yourself the question; which message do you want to send?

Based on my experience, my generation is quite nostalgic and likes to play games they played as a child. For example, Pokémon Go or just regular board games are very popular. 
This need to connect with your childhood is reflected in my game as I also mentioned earlier. There is a nostalgic element in the simplicity of the game. 
I believe this is an important part of the culture, also since almost everything we knew as children has been digitalized. 

**Examples/references:**
http://siusoon.gitlab.io/aesthetic-programming/ - Winnie Soon's sketch
https://editor.p5js.org/amanda.hansen1404/sketches/8uBgk0wI - The short sketch we created in the class
