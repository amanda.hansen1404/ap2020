//creating the variables
let min_curry = 5;  //min tofu on the screen
let curry = [];
var chick;
let chickPosY;
let chickPosX
var currybowl
let mini_height;
var smallchick;
var words = ["Oh, no","Oops","Pheew","Hurry"]
var index = 0;
let speechchick;


function preload () {
//loading the images
chick = loadImage("https://media3.giphy.com/media/anwMKSfnwuis0/giphy.gif");
currybowl = loadImage("https://scontent-arn2-1.xx.fbcdn.net/v/t1.15752-9/88416914_748916945636516_8820758370848866304_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_ohc=Pk15g_QivlMAX_IgAA1&_nc_ht=scontent-arn2-1.xx&oh=1f272a34235bb4287174a9bab2450a31&oe=5E937A7A")
speechchick = loadImage("https://scontent-lht6-1.xx.fbcdn.net/v/t1.15752-9/89273926_1856766527788596_3032873851647164416_n.png?_nc_cat=106&_nc_sid=b96e70&_nc_ohc=5nrJY58DYesAX_qcty4&_nc_ht=scontent-lht6-1.xx&oh=3a8ed5bc1a57979741a4bc94073df82e&oe=5E914E44")
}

function setup() {
  createCanvas(500, 500);
  chickPosY = height/2;
  chickPosX = width/2;
   frameRate(50);

 mini_height = height/0.5;
   for (let i=0; i<=min_curry; i++) {
    curry[i] = new Curry(); //create/construct a new object instance
  }

}

function draw() {
  background('rgb(232,232,135)');
//for (let i = 0; i <curry.length; i++) { //making sure the curry bowls move

 //curry[i].move();
 //curry[i].show();
//}

  //calling the functions
 showCurry();
 checkCurryNum();
   //images

   //the talking chick
   speechchick.resize(150,0);
   image(speechchick, 335, 5);

  //the chicken wearing a hat
  chick.resize(60,0);
    image(chick, chickPosX, chickPosY);

    //text

    //header
    fill(0);
    textSize(30);
    textStyle(BOLDITALIC);
    text("Don't become curry",170,50);

    //rules
    fill(0);
    textSize(15);
    textStyle(NORMAL);
    textAlign(LEFT);
    text("Rules: avoid the curry bowls",10,465);
    text("How: use the arrow keys to move",10,485);

    //speech bubble
    textSize(12);
    textAlign(CENTER);
    fill(0);
    text(words[index], 380,40);

}

function showCurry(){
 for (let i = 0; i <curry.length; i++) {
    curry[i].move();
    curry[i].show();
  }
}

function checkCurryNum() {
 if (curry.length < min_curry) {
  curry.push(new Curry());
 }
}

//the class for the curry bowl images
class Curry {
  constructor() {
  this.speed = random(0.5,2); //the curry bowls move slowly
  this.size = random(30,50); //the sizes of each curry bowl is different
  this.pos= new createVector(width+5,300,);
  // this.rotate = random(0,PI/20); //rotate in clockwise for +ve no

  }
  move() {
  this.pos.x-=this.speed; //making the curry bowls move along the x-axis
  //this.pos.y-=this.speed;
  }
  show() {
 image(currybowl, this.pos.x,this.pos.y,this.size,this.size); //the curry bowls appear on the screen

  }
    }


function keyPressed() {
  //the game controls

    if (keyCode === UP_ARROW) {
      chickPosY-=40;
    } else if (keyCode === DOWN_ARROW) {
      chickPosY+=40;
    }

    if (keyCode === LEFT_ARROW) {
      chickPosX-=40;
    } else if (keyCode === RIGHT_ARROW) {
      chickPosX+=40;
    }

    //when you press the key, the words in the speech bubble change
    index = index + 1;

    if (index==4) {
      index = 0;
    }

}
