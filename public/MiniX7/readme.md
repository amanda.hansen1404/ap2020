## MiniX 7 by Amanda Hansen and Katrine Holm Kjærgaard


**The Runme:** [Link](http://KatrineHK.gitlab.io/ap2020/MiniX7/)

**The repository for the program has been uploaded on Katrine's profile:**
[Link for the repository for the program](https://gitlab.com/KatrineHK/ap2020/-/tree/master/public/MiniX7)
Go this page to read our readme and the sketch

*Note:* The sketch I have uploaded is not the finished one. - Check out Katrine's page. 