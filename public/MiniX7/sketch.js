//these variables have been placed here, because then they work globally
var stars = [];
var stars2 = [];
var slider;
let starimage;
let starimage2;
let mickeymouse;

var backgrounds = ['#000033', '#002080', '#000080', '#002266'];
var index = 0;


  // the star shape we've used
function preload() {
starimage = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/84507792_227883251689595_5312078004812251136_n.png?_nc_cat=107&_nc_sid=b96e70&_nc_ohc=EAAOMJ6C4JgAX9k0hz1&_nc_ht=scontent.faar2-1.fna&oh=1444428599e125ce01cb599fa220b1a9&oe=5E98DC19");
starimage2 = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90529673_214515679771065_933834329484689408_n.png?_nc_cat=107&_nc_sid=b96e70&_nc_ohc=9aqkSx_Upn4AX_wvL13&_nc_ht=scontent.faar2-1.fna&oh=70f65341d07f7d4a51bd8c0f7b3a07a5&oe=5E9B3C8B");
mickeymouse = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90536863_2920596194835468_8600110511985524736_n.png?_nc_cat=111&_nc_sid=b96e70&_nc_ohc=9gMIxjRx4lUAX8Liv7M&_nc_ht=scontent.faar2-1.fna&oh=44e77aa90b50793aa4006c8df10c4bea&oe=5E99F753");

}
function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight);
//framrate is set to 10, to get the blinking to slow down
  frameRate(8);
  //a for loop that creates new stars. The limit has been set to 80
  for (var i = 0; i<80;i++){
    stars[i]=new Star();
  }

  //creating the slider
  slider = createSlider(1, 50, 20);
  slider.position(20,80);
  slider.style('width', '80px');

  noCursor();

}

function draw() {
  // put drawing code here
  background(backgrounds[index]);
  let val = slider.value();
  for (var i = 0; i<30;i++){
    stars[i].show();
    //this makes sure that when you use the slider, the stars will change sizes
    stars[i].changeSize(val);
  }

  //getting the blinking effect for Star2
  for (var i = 0; i<40;i++){
    stars2[i]=new Star2();
    stars2[i].show1();
  }

  //text - instructions
push();
  fill('white');
  textStyle(BOLDITALIC);
  textSize(25);
  text('Press Mickey', 10, 50);
pop();

  //the mickey mouse
push();
  mickeymouse.resize(110,0);
  imageMode(CENTER);
  image(mickeymouse,mouseX,mouseY);
pop();

}
//a class, that makes sure that the stars always look the same
class Star {
	constructor() {
    this.size= random(1,50);
    this.startSize=this.size
    this.xpos= random(width);
    this.ypos= random(height);
}

//this function makes sure that the stars are shown
  show() {
    image(starimage, this.xpos, this.ypos, this.size, this.size);

  }

  //this function can make the star change sizes
  changeSize(val){
    this.size = this.startSize + val

  }
}
  //a second class for the blinking stars
class Star2 {
	constructor() {
    this.size= random(1,50);
    this.startSize=this.size
    this.xpos= random(width);
    this.ypos= random(height);
}

//this function makes sure that the stars are shown
  show1() {
    image(starimage2, this.xpos, this.ypos, this.size, this.size);

  }
}
//changing the background when the mouse is pressed
function mousePressed() {
  index = index + 1;

  if (index==4) {
    index = 0;
  }
}
